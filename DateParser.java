import java.time.LocalDate;
import java.time.format.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Use regex to find date in an input string
 * Then parse them into a list of LocalDate and a Map of occurence.
 *
 * @author PH
 */
public class DateParser {
    private static final String REGEX_PATTERN1 = "\\w* [0-3][0-9], \\d{4}";
    private static final String REGEX_PATTERN2 = "[0-3][0-9] \\w* \\d{4}";
    private static final String REGEX_PATTERN3 = "\\d{4}-\\d{2}-\\d{2}";
    private static final String DATE_PATTERN1 = "MMMM dd, yyyy";
    private static final String DATE_PATTERN2 = "dd MMMM yyyy";
    private static final String DATE_PATTERN3 = "yyyy-MM-dd";

    private String input = "Marvin Lee Minsky at the Mathematics Genealogy Project; 20 May 2014\n" +
            "Marvin Lee Minsky at the AI Genealogy Project. {reprint 18 September 2011)\n" +
            "\"Personal page for Marvin Minsky\". web.media.mit.edu. Retrieved 23 June 2016.\n" +
            "Admin (January 27, 2016). \"Official Alcor Statement Concerning Marvin Minsky\".\n" +
            "    Alcor Life Extension Foundation. Retrieved 2016-04-07.\n" +
            "\"IEEE Computer Society Magazine Honors Artificial Intelligence Leaders\".\n" +
            "    DigitalJournal.com. August 24, 2011. Retrieved September 18, 2011.\n" +
            "    Press release source: PRWeb (Vocus).\n" +
            "\"Dan David prize 2014 winners\". May 15, 2014. Retrieved May 20, 2014.";
    private List<String> dateStringList;
    private List<LocalDate> allDateList;
    private List<LocalDate> dateList;
    private Map<LocalDate, Integer> dateOccurrenceMap;

    public DateParser() {
        dateStringList = new ArrayList<>();
        allDateList = new ArrayList<>();
        dateList = new ArrayList<>();
        dateOccurrenceMap = new HashMap<>();
    }

    public static void main(String[] args) {
        DateParser dateParser = new DateParser();
        dateParser.process();
    }

    /**
     * Process the input string then print the output
     */
    public void process() {
        findDateInString();
        parseDate();
        sortLocalDateList();
        printOutput();
    }

    /**
     * Find regex pattern in input
     * List result in List<String> dateStringList
     */
    private void findDateInString() {
        Pattern pattern = Pattern.compile(REGEX_PATTERN1 + "|" + REGEX_PATTERN2 + "|" + REGEX_PATTERN3);
        Matcher matcher = pattern.matcher(input);

        while (matcher.find()) {
            this.dateStringList.add(matcher.group());
        }
    }

    /**
     * Find date pattern in List<String> dateStringList
     * List result in List<LocalDate> allDateList
     */
    private void parseDate() {
        if (dateStringList.isEmpty()) {
            return;
        }

        DateTimeFormatter formatter = new DateTimeFormatterBuilder()
                .appendOptional(DateTimeFormatter.ofPattern(DATE_PATTERN1))
                .appendOptional(DateTimeFormatter.ofPattern(DATE_PATTERN2))
                .appendOptional(DateTimeFormatter.ofPattern(DATE_PATTERN3))
                .toFormatter();

        for (String dateString : dateStringList) {
            try {
                LocalDate localDate = LocalDate.parse(dateString, formatter);
                allDateList.add(localDate);
            } catch (DateTimeParseException e) {
                System.out.println("Parsing for this date : " + dateString + " isn't supported.");
            }
        }
    }

    /**
     * Find occurrence in List<LocalDate> allDateList
     * Put number of occurence in Map<LocalDate, Integer> dateOccurrenceMap
     * AND sort unique date in List<LocalDate> dateList
     */
    private void sortLocalDateList() {
        if (allDateList.isEmpty()) {
            return;
        }

        for (LocalDate date : allDateList) {
            if (dateOccurrenceMap.containsKey(date)) {
                Integer oldValue = dateOccurrenceMap.get(date);
                dateOccurrenceMap.put(date, oldValue + 1);
            } else {
                dateList.add(date);
                dateOccurrenceMap.put(date, 1);
            }
        }

        // Sort from oldest date
        Collections.sort(dateList);
    }

    /**
     * Print in console like this :
     * 2011:
     * -08
     * -24 (1)
     * -09
     * -18 (2)
     */
    private void printOutput() {
        if (dateList.isEmpty()) {
            System.out.println("0 date found in the input string.");
            return;
        }

        LocalDate oldDate = null; // null for the 1st case dateList.get(0)
        for (LocalDate date : dateList) {
            if (oldDate != null && date.getYear() == oldDate.getYear()) {
                if (date.getMonthValue() == oldDate.getMonthValue()) {
                    printDay(date);
                } else {
                    printMonth(date);
                    printDay(date);
                }
            } else {
                printYear(date);
                printMonth(date);
                printDay(date);
            }

            oldDate = date;
        }
    }

    private void printYear(LocalDate date) {
        System.out.println(date.getYear() + ":");
    }

    private void printMonth(LocalDate date) {
        System.out.println("\t-" + String.format("%02d", date.getMonthValue()));
    }

    private void printDay(LocalDate date) {
        System.out.println("\t\t-" + String.format("%02d", date.getDayOfMonth()) + " (" + dateOccurrenceMap.get(date) + ")");
    }

    public void setInput(String input) {
        this.input = input;
    }
}
